# Group meetings #

## Schedule

| Date | Who | Topic | Remark |
| :--- | :-- | :---- | ------ |
|26.06.2019 | Fritz, Mai | Bachelor defence | |
|03.07.2019 | Everyone | 30 seconds elevator talk | two small groups, two rooms |
|10.07.2019 | Timothy | Poster design | |
|17.07.2019 | Sebastian | Cluster extreme programming | |
|24.07.2019 | Sergey, Sebastian | Entropy day | |
|31.07.2019 | Isaac, Yufei | Coop presentation | |
|07.08.2019 | Farid | UQLab introduction | |
|14.08.2019 | Everyone | Dream figure | |
|21.08.2019 | Everyone | BBQ | |



## Upcoming topics

* Wolfgang, Jannik: Multi-objective optimization
* Ana: Water divide
* Wolfgang: Efficiently programming with Matlab
* September 2019 -> Job distribution

## Regular topics

* [once a year] Teaching: Exchange of methods, how to create interactivity,  checking for tasks in the list of group goals
* [once a year] Job distribution
* [once a year] Communicate group goals, check group goal fulfillment (maybe on two meetings)

## Possible future topics
* Elevator talks:
    * Everyone
* Group discussion:
    * What is the purpose of conferences? Posters? Presentations?
    * Check back on Master thesis topics (Data driven ones)
* Extreme Programming
    * Particle filter
    * Gradient search
* Supervision Guidelines
* Reihum-Vorträge, z.B.
    * Entropy-Article, Wolfgang's Master Thesis (Anneli, Wolfgang) Soon!
* Sergey: new stuff like Bitcoins
* ??? - Publishing software on our website - license and more


### History

#### 2019

| Date | Who | Topic | Remark |
| :--- | :-- | :---- | ------ |
|09.01.2019 | | | |
|16.01.2019 | Gözde and everyone | LS3-Days brainstorming | |
|23.01.2019 | Wolfgang | How to write a review | |
|30.01.2019 | Sinan | Summarize his latest paper | |
|06.02.2019 | | no meeting - everyone at ENWAT seminar | |
|13.02.2019 | Marvin | defense practice talk | Wolfgang not here |
|20.02.2019 | Sergey | Teaching round table | |
|27.02.2019 | Barbara | Decision making | |
|06.03.2019 | Wolfgang and Sergey | Job distribution | |
|13.03.2019 | | no meeting (too close to LS3-days | |
|20.03.2019 | | no meeting (SFB1313) | Wolfgang not here |
|27.03.2019 | Diego | A stress-test to evaluate hydrological model robustness under climate variability | Guest Diana Spieler here |
|03.04.2019 | Everyone | EGU Practice | Wolfgang not here |
|10.04.2019 | | no meeting (EGU) | |
|17.04.2019 | Ishani and Timothy | LS3-Days Presentation | 13:30 mini-milestone Siddharth (Reynold) |
|24.04.2019 | Jannik | Leitfaden Prüfungseinsicht | |
|01.05.2019 | | no meeting, public holiday | |
|08.05.2019 | Sebastian | Level set method | |
|15.05.2019 | Reynold | PhD Defense Practice (Starts at 11:00) | SimTech Kontaktmesse on that day |
|22.05.2019 | | | |
|29.05.2019 | Micha, Wolfgang, Jannik | Presentation tricks | |
|05.06.2019 | | | |
|12.06.2019 | | | |
|19.06.2019 | | | |
|26.06.2019 | | | |

#### 2018
| Date | Who | Topic | Remark |
| :--- | :-- | :---- | ------ |
|10.01.2018 | Micha | Setting up a schedule for group meetings | |
|17.01.2018 | | | |
|24.01.2018 | Jochen | Introduce himself and show his work | |
|31.01.2018 | Wolfgang | Group goals: Review performance in 2017 | |
|07.02.2018 | Wolfgang | Group goals: Discuss and set goals for 2018 | |
|14.02.2018 | | Group goals: Discuss and set goals for 2018 (continued) | |
|21.02.2018 | Jannik | Present his work | |
|28.02.2018 | everyone | Group goals: Discuss and set goals for 2018 (continued) | |
|07.03.2018 | - no meeting - | | |
|14.03.2018 | Wolfgang | Extreme Programming: Kriging | |
|21.03.2018 | Everyone | Discuss Feedback from LS3-Day, Formulate Poster Text | 13:30: Mini-Milestone, Timo |
|28.03.2018 | | SimTech Conference | |
|04.04.2018 | | Hydromod Conference | |
|11.04.2018 | | EGU Conference | |
|18.04.2018 | Sebastian | Finding a title for paper | |
|25.04.2018 | Rachael and Meghan | Present their work | |
|02.05.2018 | Wolfgang | How to do a PhD | |
|09.05.2018 | Jannik, Marvin | 30 seconds elevator talk | Tübingen Coops introduce themselves |
|16.05.2018 | Wolfgang | How to do a PhD, summary and final discussion | |
|23.05.2018 | | No Meeting | Wolfgang on Vacation |
|30.05.2018 | Anneli | Extreme Programming: Bayesian Model Selection | 13:30 - Minimilestone Juan (Sergey) |
|06.06.2018 | | No Meeting (CMWR) | Wolfgang not here (teaching until 11:15, Stadtmitte) |
|13.06.2018 | Ana | Present her work | |
|20.06.2018 | Aline, Farid | 30 seconds talk | 13:30 - Minimilestone Raphael; Wolfgang not here until ~12:00 (teaching) |
|27.06.2018 | Michael | Group goals | |
|04.07.2018 | Jannik, Ana | Course recap: Teaching in English | |
|11.07.2018 | Teng, Sebastian R. | 30 seconds talk| Wolfgang not here (teaching until 11:15, Stadtmitte) |
|18.07.2018 | - | No Meeting (ENWAT Doctoral Seminar) | |
|25.07.2018 | | Mini Milestones | |
|01.08.2018 | Michael | Group goals | |
|08.08.2018 | | End-of-Term-Presentation Coop Students (Tübingen + Stuttgart) | Meeting start at 11:00 |
|15.08.2018 | | No Meeting | |
|22.08.2018 | | No Meeting | Wolfgang on vacation |
|29.08.2018 | | No Meeting | Wolfgang on vacation |
|05.09.2018 | Jeremy Bennett | Guest Presentation (Uni Tübingen)  | |
|12.09.2018 | | Scheduling | |
|19.09.2018 | Sergey | Task distribution (including teaching) | |
|26.09.2018 | Uwe Ehret (KIT), Guest | Data-based learning and predicion using a frequentist interpretation of probability and measures from Information Theory | |
|03.10.2018 | | no meeting - public holiday | |
|10.10.2018 | | no meeting - Hydromod fall school | |
|17.10.2018 | Sergey | Introduction to smh-server (clean up before if necessary) | |
|24.10.2018 | Sebastian R., Aline, Micha| Introduction to git | |
|31.10.2018 | Marvin| Defining sub-groups| |
|07.11.2018 | Sebastian R., Teng| Present their work | 13:30 - Mini-Milestone Presentation by Jan Völter (in German) |
|14.11.2018 | Sinan| Present his work | |
|21.11.2018 | Gabi, Darynne | Final presentation | |
|28.11.2018 | | AGU Rehearsal (Posters and Presentations) | |
|05.12.2018 | Emily, Maire | Final presentation | |
|12.12.2018 | | no meeting - AGU | |
|19.12.2018 | | Celebration | |


#### 2017
| Date | Who | Topic | Remark |
| :--- | :-- | :---- | ------ |
|01.03.2017 | Jannik | Grammarly | Mini-Milestone Natalie (Jannik) |
|08.03.2017 |  | AGU Poster post-mortem | Mini-Milestone Sebastian (Jannik) |
|15.03.2017 | Felix | Special Issue Paper| |
|22.03.2017 | Marvin | What is model complexity ? | |
|29.03.2017 | Ana | Present her work | |
|12.04.2017 | Reynold | Presentation of Evan (20min + 10min questions), EGU Poster preview (5min + 15min discussion)| |
|19.04.2017 | Reynold | Modflow Magic | 11:30 - 12:15 |
|26.04.2017 |  |  | EGU (no meeting) |
|03.05.2017 | Wolfgang | Group Goals | |
|10.05.2017 | Jannik | Optimization with GAMS | Scheduling |
|17.05.2017 |  |  | No groupmeeting |
|24.05.2017 | Anneli | Risk paper | |
|31.05.2017 |  |  | No groupmeeting |
|07.06.2017 |  |  | No groupmeeting |
|14.06.2017 | Group, Wolfgang, Marvin, Abelardo | Bachelor Topics, SFB Helfer, TDW Helfer, Publication-List Check |  |
|21.06.2017 | Jannik, Marvin, Reynold, Sergey | Reviewlets, Scheduling | Bam items: the right use of our group calenders, ENWAT doctoral seminar: who wants to present? Abstracts AGU |
|28.06.2017 |  |  | No groupmeeting |
|05.07.2017 |  | Aline: data collection and presentation, ideas of topics | |
|12.07.2017 |  | Minimilestones, Claudius (Sebastian) and Delia (Jannik)  | Wolfgang is late |
|19.07.2017 |  | no meeting | Micha is late |
|26.07.2017 | Hugues, Christine (Sebastian) | Final presentation | |
|02.08.2017 | | | |
|23.08.2017 | | | |
|06.09.2017 | | Minimilestone Yoga und Tulio (Jannik)  | |
|13.09.2017 | everyone | Gathering of Bachelor and Master thesis topics + Verhaltenskodex zur Korruptionsprävention | |
|20.09.2017 | Abelardo | Particle Swarm Optimization | |
|27.09.2017 | Micha | Miracle and False Miracles | |
|04.10.2017 | Sergey, Felix | SMH, where is what?, Meistertask, what is it? | |
|11.10.2017 | Tomas Aquino (Guest) | Chemical Continuous Time Random Walks | |
|18.10.2017 | everyone | collect topics for students' theses | |
|25.10.2017 | | | |
|01.11.2017 | | | |
|08.11.2017 | Sebastian | Planning of LS3-Day | |
|15.11.2017 | Wolfgang |Extreme Programming MCMC | |
|22.11.2017 | Reynold | Practice AGU talk | |
|29.11.2017 | Marvin, Sebastian, Jannik | AGU Poster review | Wolfgang not here |
|06.12.2017 | | | Wolfgang not here |
|13.12.2017 | | no meeting, AGU | Wolfgang not here |
|20.12.2017 | Wolfgang | Review group goal performance, cake (everyone) | |

#### 2016 

* 13.01. Set up Schedule
* 20.01. Programmdemo (Felix), Make a Schedule for next weeks
* 27.01. entfällt -> Tübingen
* 01.02. (Montag) Probevortrag Anneli 9:30
* 03.02. Entfällt -> Alle in TÃ¼bingen
* 10.02. Julian - Hypothesis Testing + Credibility Interval vs. Confidence Interval
* 17.02. 30s: Micha + Dissertation Title,
* 24.02. 30s: Abelardo and Julian
* 02.03. Felix - project title, group picture
* 09.03. Presentation (Reynold)
* 16.03. 30s: Abelardo
* 23.03. (Wolfgang not here) 30s: Julian
* 30.03. (Wolfgang not here) Introduction to PCE: Sergey
* 06.04. 
* 13.04. Julian and everybody: Backup party
* 20.04. - no meeting, EGU -
* 27.04. Anneli: Project proposal
* 04.05. Tag der Wissenschaft, IWS-Vollversammlung, Julian: Paper title
* 11.05. Felix: Paper title
* 18.05. Abelardo: transient catchments
* 25.05. Julian Summary of Paper (if applicable) + Error modeling 
* 01.06. Bucket-Model Session (Reynold, Marvin)
* 08.06. Jannik, Extension planing
* 15.06. Tag der Wissenschaft
* 22.06. Wolfgang - Report von Schulung
* 29.06. Micha - Surrogate modeling
* 06.07. Nelson
* 13.07. Marvin BME
* 20.07. Mini-Milestone Raphael
* 27.07. Mini-Milestone
* 03.08. Melanie
* 10.08. 
* 17.08. Anneli - Paper-Titel-Findung
* 24.08.
* 31.08. Ideas for Anjas present, Software für die Lehre aus Qualitätssicherungsmitteln
* 14.09. Sebastian - Milestone-Testvortrag   [Vor-Ort-Begutachtung Tübingen]
* 21.09. Wolfgang und alle - "Risiko"
* 28.09. Wolfgang - Group Goals (cont'd)
* 05.10. Anneli - Choosing between thermodynamic models with Bayesian model selection
* 12.10. no meeting (rooms are taken)
* 19.10. Felix - Studentenarbeits-Bewertungsschema
* 26.10. no meeting
* 09.11. Wolfgang - Ty Ferre group discussion and maybe group goals (continued)
* 16.11. Betsaida Master's thesis presentation 

#### 2015

* 28.10. 30s: Sebastian, Anneli / Die Große Podiums-Kopula-Diskussion - Andi / New SMH Structure
* 04.11. 30s: Jannik, Sebastian / Informal Talk: MCMC - Wolfgang
* 11.11. 30s: Jannik, Marvin, Reynold / Stochastic Optimization in Power Systems - Jannik
* 18.11. 30s: Sergey, Wolfgang / Code of Conduct
* 25.11. (Status Seminar SimTech)
* 02.12. 30s: Felix / MitarbeitergesprÃ¤che Guidelines
* 09.12. 30s: Micha / Micha: Selbstplagiat
* 16.12. (AGU) - no meeting

#### 2014
* 27.10. Hypothesis exercise, Title exercise
* 03.11. Scientific Writing II
* 10.11. Sebastian
* 17.11. Anneli
* 24.11. entfällt
* 01.12. Codearchivierung (Julian und Micha)
* 08.12. Georgenhof / Wie rechnet man auf dem Cluster? / Umzug
* 15.12. entfällt - AGU
* 22.12. entfällt - keiner da
* 12.01. Präsentationsvorlagen - Micha
* 19.01. Logo - Julian / Layout Folien - Micha / Austausch: Softskillkurse - alle
* 26.01. How to present your work - Sebastian + Sergey
* 02.02. Versionskontrolle - Andi + Julian / Code teilen (Stand der Dinge)
* 09.02. entfällt - Wolfgang in Aachen
* 16.02. Was verspreche ich mir von einer Betreuungsbeziehung? - Felix fragt Anja oder macht es.
* 23.02. Ultraglattes Paper - Sergey
* 16.03. Überblick Bitchjobs, Journalwatch, Betreuungsverhältnis
* 23.03. Regeln für Klausuraufgaben, Laufendeanträge
* 30.03.
